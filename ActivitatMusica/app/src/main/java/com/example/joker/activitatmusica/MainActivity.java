package com.example.joker.activitatmusica;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AudioAttributes audioAttributes;
    private SoundPool soundPool;
    private SoundPool soundPool2;
    private Button buttonSound1, buttonSound2;
    private Button buttonStop;
    private int idFx1, idFx2;
    private int nowPlaying, nowPlaying2;
    private SeekBar seekBar;
    private Spinner spinner;
    private AudioManager audioManager;

    private List<Integer> llista = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        retrieveViews();
        createAudioManager();
        createAudio();
        loadMusic();
        setEvents();


    }

    private void createAudioManager(){
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.opcions_click, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


      audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

    }
    private void createAudio() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            soundPool = new SoundPool.Builder()
                    .setMaxStreams(5)
                    .setAudioAttributes(audioAttributes)
                    .build();

            soundPool2 = new SoundPool.Builder()
                    .setMaxStreams(5)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            //
            soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
            soundPool2 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);


        }
    }

    private void loadMusic() {
        try {
            AssetManager assetManager = this.getAssets();
            AssetFileDescriptor descriptor;
            descriptor = assetManager.openFd("1.mp3");
            idFx1 = soundPool.load(descriptor, 0);

            AssetFileDescriptor descriptor2;
            descriptor2 = assetManager.openFd("2.mp3");
            idFx2 = soundPool2.load(descriptor2, 0);

        } catch (IOException e) {

        }
    }


    private void retrieveViews() {
        spinner = findViewById(R.id.spinner);
        buttonSound1 = findViewById(R.id.so1);
        buttonStop = findViewById(R.id.buttonStop);
        seekBar = findViewById(R.id.controlVolum);
        buttonSound2 = findViewById(R.id.so2);
    }

    private void setEvents() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                soundPool.setVolume(idFx1, i, i);
                soundPool2.setVolume(idFx2, i, i);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        buttonSound1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                llista.add(soundPool.play(idFx1, 1, 1, 0, 0, 1));

            }
        });

        buttonSound2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nowPlaying2 = soundPool2.play(idFx2, 1, 1, 0, 0, 1);

            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Integer i : llista) {
                    soundPool.stop(i);

                }
                soundPool2.stop(nowPlaying2);

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                 int position = spinner.getSelectedItemPosition();
                switch (position) {
                    case 0:
                        audioManager.playSoundEffect(SoundEffectConstants.CLICK);
                        break;
                    case 1:
                        audioManager.playSoundEffect(SoundEffectConstants.NAVIGATION_DOWN);
                        break;
                    case 2:
                        audioManager.playSoundEffect(SoundEffectConstants.NAVIGATION_RIGHT);
                        break;
                    case 3:
                        audioManager.playSoundEffect(SoundEffectConstants.NAVIGATION_UP);
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
    }
}




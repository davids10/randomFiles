import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Properties;
import java.util.Scanner;

public class Main {
	
	private static String url = "jdbc:mysql://localhost:3306/sakila";
	private static Properties connectionProperties = new Properties();
	private static boolean hasResults=false;


	public static void main(String[] args) {
		connectionProperties.setProperty("user", "david");
		connectionProperties.setProperty("password", "12345");
		
		consulta6();
	}
	
	private static void consulta1(){
		
		try (Connection con = DriverManager.getConnection(url,connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Connexió a la base de dades2222!");
			try (ResultSet rs =
          st.executeQuery("SELECT a.first_name,a.last_name,f.title FROM film f JOIN film_actor fa ON f.film_id=fa.film_id JOIN actor a ON fa.actor_id=a.actor_id  WHERE f.title='TWISTED PIRATES' ");) {
				while (rs.next()) {
					if (!hasResults) {
						hasResults=true;
						System.out.println("Llista:");
					}
					String actors = rs.getString(1);
					String nomPeli = rs.getString(2);
					String a = rs.getString(3);
					System.out.println(nomPeli + " " + actors  + " peli " + a );
				}
				if (!hasResults) {
					System.out.println("No hi ha dades a la taula");
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}
	
	private static void consulta2(){
		try (Connection con = DriverManager.getConnection(url,connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Connexió a la base de dades!");
			try (ResultSet rs =
          st.executeQuery("SELECT length,title, description FROM film WHERE length>150");) {
				while (rs.next()) {
					if (!hasResults) {
						hasResults=true;
						System.out.println("Llista:");
					}
					int duracio = rs.getInt(1);
					String titul = rs.getString(2);
					String descripcio = rs.getString(3);
					System.out.println(duracio + " " + titul+" "+descripcio);
					//System.out.println(titul+" "+descripcio);
				}
				if (!hasResults) {
					System.out.println("No hi ha dades a la taula");
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}		
	}
	private static void consulta3(){
		try (Connection con = DriverManager.getConnection(url,connectionProperties);
                Statement st = con.createStatement();) {
            System.out.println("Connexió a la base de dades4!");
            try (ResultSet rs =
          st.executeQuery("SELECT st.store_id,s.first_name,s.last_name,a.address " + 
			          		"FROM address a " + 
			          		"JOIN staff s ON s.address_id = a.address_id " + 
			          		"JOIN store st ON s.staff_id = st.manager_staff_id ");) {
                while (rs.next()) {
                    if (!hasResults) {
                        hasResults=true;
                        System.out.println("Llista:");
                    }
                    int botiga = rs.getInt(1);
                    String nom = rs.getString(2);
                    String cognom = rs.getString(3);
                    String adresa = rs.getString(4);
                    System.out.println("botiga " + botiga + " mangaer " + nom + " " + cognom + " adresa botiga " + adresa);
                }
                if (!hasResults) {
                    System.out.println("No hi ha dades a la taula");
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }
	}
	private static void consulta4(){
		try (Connection con = DriverManager.getConnection(url,connectionProperties);
                Statement st = con.createStatement();) {
            System.out.println("Connexió a la base de dades3!");
            try (ResultSet rs =
          st.executeQuery("SELECT c.first_name, c.last_name,i.store_id,i.inventory_id,r.return_date "
          		+ "FROM customer c "
          		+ "JOIN store s ON s.store_id=c.store_id JOIN inventory i ON i.store_id=s.store_id JOIN rental r ON r.inventory_id = i.inventory_id  "
          		+ "WHERE c.first_name='ALLISON' && c.last_name='STANLEY' && r.return_date!='NULL' ");) {
                while (rs.next()) {
                    if (!hasResults) {
                        hasResults=true;
                        System.out.println("Llista:");
                    }
                    String nomClient = rs.getString(1);
                    String cognomClient = rs.getString(2);
                    int tenda = rs.getInt(3);
                    int item = rs.getInt(4);
                    LocalDate dataRetorn = rs.getDate(5).toLocalDate();
                    System.out.println(nomClient + " " + cognomClient + " tenda " +  tenda + "  item " + item + " retorn " + dataRetorn);
                }
                if (!hasResults) {
                    System.out.println("No hi ha dades a la taula");
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }
	}
	
	private static void consulta5 (){
		try (Connection con = DriverManager.getConnection(url,connectionProperties);
                Statement st = con.createStatement();) {
            System.out.println("Connexió a la base de dades6!");
            try (ResultSet rs =
          st.executeQuery("SELECT COUNT(*),c.name " + 
		          		"	FROM film f " + 
		          		"	JOIN film_category fc ON f.film_id = fc.film_id " + 
		          		"	JOIN category c ON c.category_id = fc.category_id " + 
		          		"	GROUP BY c.category_id; ");) {
                while (rs.next()) {
                    if (!hasResults) {
                        hasResults=true;
                        System.out.println("Llista:");
                    }
                    int numeroPelis = rs.getInt(1);
                    String categoria = rs.getString(2);
                    System.out.println(" numeroPelis " + numeroPelis + " categoria " + categoria);
                }
                if (!hasResults) {
                    System.out.println("No hi ha dades a la taula");
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }

	}

	
	
	private static void consulta6(){
		System.out.println("Exercici4ConsultesParametres");
		
		Scanner sc = new Scanner(System.in);
		String paraulaClau=" ";
		try (Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/sakila", "david", "12345");
			PreparedStatement st = conn.prepareStatement(
					"SELECT f.title,f.description,s.store_id " + 
					"FROM film f " + 
					"JOIN inventory i ON f.film_id=i.film_id " + 
					"JOIN store s ON s.store_id=i.store_id " + 
					"WHERE f.title LIKE ? OR f.description LIKE ? ");) {
			while (!paraulaClau.equals("")) {
				System.out.println("paraula clau a buscar en titul o descripcio (en blanc per sortir): ");
				paraulaClau = sc.nextLine();					
				if (!paraulaClau.equals("")) {
					st.setString(1, "%"+paraulaClau+"%");
					st.setString(2, "%"+paraulaClau+"%");
					try (ResultSet rs = st.executeQuery();) {
						while (rs.next()) {
							String nomPeli = rs.getString(1);
		                    String descripcioPeli = rs.getString(2);
		                    int tenda = rs.getInt(3);	                   
		                    System.out.println(nomPeli + " " + descripcioPeli + " tenda " +  tenda);
						}
					}
				}
			}
		
		} catch (SQLException e) {
			System.err.println("Error SQL: "+e.getMessage());
		}
		sc.close();

	}
	
	private static void consulta7(){
		System.out.println("Exercici1ConsultesParametres");
		
		Scanner sc = new Scanner(System.in);
		String lastName=" ";
		try (Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/sakila", "david", "12345");
			PreparedStatement st = conn.prepareStatement(
					"SELECT f.title " + 
					"FROM film f " + 
					"JOIN film_actor fa ON fa.film_id = f.film_id " + 
					"JOIN actor a ON a.actor_id = fa.actor_id " + 
					"WHERE a.last_name LIKE ?");) {
			while (!lastName.equals("")) {
				System.out.println("Cognom a cercar (en blanc per sortir): ");
				lastName = sc.nextLine();
				if (!lastName.equals("")) {
					st.setString(1, lastName);
					try (ResultSet rs = st.executeQuery();) {
						while (rs.next()) {
							String nomPeli = rs.getString(1);
							System.out.println(nomPeli);
						}
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: "+e.getMessage());
		}
		sc.close();

	}
	
}

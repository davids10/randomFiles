import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;

import static com.mongodb.client.model.Accumulators.*;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class RepasMongo {

	public static int contador = 0;
	public static double examenMitjana = 0;
	public static double deuresMitjana = 0;
	public static double testosMitjana = 0;
	public static double notaFinal = 0;
	public static final String EXAMEN = "Examen";
	public static final String ACTIVITATS = "Activitats";
	public static final String TESTOS = "Testos";
	public static int idClasse =0;
	public static double notaMaxima = 0.0;
	public static boolean primerCop = true;
	
	public static MongoClient client = new MongoClient();
	public static MongoDatabase db = client.getDatabase("test");
//	public static MongoCollection<Document> coll = db.getCollection("students");
	public static MongoCollection<Document> coll = db.getCollection("gradles");
	private static Integer millorClasse;
	
	
	public static void main(String[] args) {

	}
	
	public static void consulta1() {
		String city = "AGAWAM";
		Bson filter = and(eq("city", city));
		Bson include = new Document("_id", 1);

		coll.find(filter).projection(include).forEach((Document doc) ->{
				System.out.print("codi zip ciutat " + city +  "  " + doc.getString("_id")+" ");
				
		});
		client.close();
	}
	
	public static void consulta2() {
		Bson include = new Document("pop", 1).append("_id", 1);
		Bson ordre = new Document("pop",-1);

		coll.find().sort(ordre).projection(include).limit(10).forEach((Document doc) -> {
			System.out.println("Zip: " + doc.getString("_id") + " - Pop: " + doc.getInteger("pop"));
	    });
		
		client.close();
	}
	
	public static void consulta3() {
		int count = (int)coll.count();
		Bson ordre = new Document("loc.1",-1);
		
		Document doc = coll.find().sort(ordre).skip(count/2).first();
		System.out.println(doc.toJson());
		
		@SuppressWarnings("unchecked")
		List<Double> loc = (List<Double>) doc.get("loc");
		System.out.println(doc.getString("_id")+": "+doc.getString("city")+", "+doc.getString("state")+
				" ("+loc.get(0)+", "+loc.get(1)+")");
		
		client.close();
	}
	
	public static void consulta4() {
		Bson filter = (lt("pop", 50));
		Bson ordre = new Document("city",1);
		Bson include = new Document("pop", 1).append("city", 1);


		coll.find(filter).sort(ordre).projection(include).forEach(
				(Document doc) -> System.out.println("City: "+doc.getString("city")+" - Pop: "+doc.getInteger("pop")));

		client.close();
	}
	
	public static void consulta5() {
		Bson ordre = new Document("_id",1);
		Bson ordre2 = new Document("_id",-1);

		List<String> states = coll.distinct("state", String.class).into(new ArrayList<String>());
		Collections.sort(states);
		for (String state : states) {
			Document first = coll.find(eq("state", state)).sort(ordre).first();
			Document last = coll.find(eq("state", state)).sort(ordre2).first();
			System.out.println("Estat "+state);
			System.out.println(" Codi postal més baix: "+first.getString("_id")+" ("+first.getString("city")+")");
			System.out.println(" Codi postal més alt: "+last.getString("_id")+" ("+last.getString("city")+")");
		}
		client.close();
	}
	
	public static void consulta6() {
		int sum = 0;
		Bson ordre = new Document("_id",1);
		Bson filter = (eq("city", "KANSAS CITY"));

		List<Document> zips = coll.find(filter).sort(ordre).into(new ArrayList<Document>());
		for (Document doc : zips) {
			System.out.println("Zip: "+doc.getString("_id")+" - Pop: "+doc.getInteger("pop"));
			sum+=doc.getInteger("pop");
		};
		System.out.println("Població total: "+sum);
		client.close();
	}
	
	public static void consulta1A() {
		coll.aggregate(Arrays.asList(
				unwind("$scores"),
				match(eq("scores.type","exam")),
				group("$name", avg("examen","$scores.score")),
				match(gte("examen", 40))
			)).forEach((Document doc) -> {
				//System.out.println(doc.toJson());
				System.out.println(doc.getString("_id")+" ("+doc.getDouble("examen")+")");
			});
			client.close();
	}
	
	public static void consulta2A() {
		coll.aggregate(Arrays.asList(
				unwind("$scores"),
				group(new Document("id","$_id").append("name", "$name").append("type", "$scores.type"), sum("quantitat", 1)),
				sort(new Document("_id.name", 1).append("_id.id", 1))
			)).forEach((Document doc) -> {
				Document id = doc.get("_id", Document.class);
				/*if (lastId!=id.getInteger("id")) {
					lastId = id.getInteger("id");
					System.out.println("Alumne "+id.getInteger("id")+" "+id.getString("name"));
				}*/
				System.out.println("  Tipus: "+id.getString("type")+"  Quantitat: "+doc.getInteger("quantitat"));
			});
			client.close();
	}
	

	
	
	

}

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static com.mongodb.client.model.Accumulators.*;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;



import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;


public class Main {

	public static MongoClient client = new MongoClient();
	public static MongoDatabase db = client.getDatabase("test");
//	public static MongoCollection<Document> coll = db.getCollection("students");
	public static MongoCollection<Document> coll = db.getCollection("gradles");

	public static int contador = 0;
	public static double examenMitjana = 0;
	public static double deuresMitjana = 0;
	public static double testosMitjana = 0;
	public static double notaFinal = 0;
	public static final String EXAMEN = "Examen";
	public static final String ACTIVITATS = "Activitats";
	public static final String TESTOS = "Testos";
	public static int idClasse =0;
	public static double notaMaxima = 0.0;
	public static boolean primerCop = true;

	
	public static boolean b = true;

	public static void main(String[] args) {

		//crearMongo();
		//consulta1();
		//consulta2();
		consulta6A();


	}

	public static void crearMongo() {
		String s = "";

		try (BufferedReader lector = new BufferedReader(new FileReader("codiPostals.txt"));) {
			while ((s = lector.readLine()) != null) {
				s = s.replaceAll(":", ",");
				String[] taula = s.split(",");
				taula[1] = taula[1].substring(2, taula[1].length() - 1);
				taula[3] = taula[3].substring(2, taula[3].length() - 1);
				taula[5] = taula[5].substring(3, taula[5].length());
				taula[6] = taula[6].substring(1, taula[6].length() - 1).trim();
				taula[8] = taula[8].trim();
				taula[10] = taula[10].substring(2, taula[10].length() - 3);

				Document document = new Document("_id", taula[1]).append("city", taula[3])
						.append("loc", Arrays.asList(Double.parseDouble(taula[5]), Double.parseDouble(taula[6])))
						.append("pop", Integer.parseInt(taula[8])).append("state", taula[10]);
				coll.insertOne(document);

			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	
	public static void consulta1() {
		// Fes un programa que demani a l'usuari el nom d'una població d'EEUU i que mostri tots els codis postals que li corresponen.
		
		Bson filter,projection;

		String s = "";
		System.out.println("-------Consulta1----------");
		Scanner sc = new Scanner(System.in);
		System.out.println("Ciutat");
		s = sc.nextLine();
		
		//System.out.println(coll.count());
		//Document first = coll.find().first();
		//System.out.println(first.toJson());
		
		filter = and(eq("city", s));
		projection = new Document("_id", 1);
		
		// 
		// Retorna tots els elements, excepte que els s'indiqui amb un 0.
		//Bson projection = new Document("x", 0).append("_id", 0);
		coll.find(filter).projection(projection).forEach((Document doc) -> System.out.println(doc.toJson()));
		


		client.close();
	}

	
	public static void consulta2() {
		// Fes un programa que mostri els 10 codis postals amb més població.
		
		Bson projection,sort;

		System.out.println("-------Consulta2----------");

		projection = new Document("_id", 1).append("pop", 1);
		sort = new Document("pop", -1);


		coll.find().projection(projection).sort(sort).limit(10).forEach((Document doc) -> System.out.println(doc.toJson()));
		
		client.close();		
	}
	
	public static void consulta3() {
		/*
		  Fes un programa que mostri a quina població pertany i a quines coordenades
			geogràfiques està situat el codi postal que està just a la mitat de totes les
			poblacions pel que fa a la seva latitud.
			
			Amb això ens referim al codi postal que té el mateix nombre de codis postals
			més al nord que més al sud.
			
			Nota: la latitud és la segona coordenada de l'array loc.
		 */
		
		Bson filter,projection,sort;

		System.out.println("-------Consulta3----------");
		
		projection = new Document("_id", 1).append("pop", 1);
		sort = new Document("pop", -1);

		coll.find().projection(projection).sort(sort).limit(10).forEach((Document doc) -> System.out.println(doc.toJson()));
		
		client.close();		
	}
	
	public static void consulta4() {
		
		/*
		   Fes un programa en Java que, a partir de la col·lecció students ens retorni
		   una llista amb tots els alumnes que han tret un 40 o més de la mitjana de tots
	       els seus exàmens.
		 */
		//System.out.println(coll.count());
		
		//Document first = coll.find().first();
		//System.out.println(first.toJson());
		
		coll.aggregate(Arrays.asList(
				unwind("$scores"),
				match(eq("scores.type", "exam")),
				group("$_id", avg("examen","$scores.score")),
				match(gte("examen", 40)),
				//project(new Document("state", 1).append("city", 1).append("_id", 0)),				
				limit(50)
				
		)).forEach((Document doc) -> System.out.println(doc.toJson()));
	}
	
	
	public static void consulta5() {
		
		/*
		  Volem saber quantes activitats de cada tipus ha realitzat cada alumne de la
		  col·lecció students. Fes un programa que ens mostri aquesta informació
		  per a cada estudiant, ordenant-los pel seu nom.
		 */
	
		
		coll.aggregate(Arrays.asList(
				unwind("$scores"),
				//match(eq("scores.type", "exam")),
				group(new Document("id","$_id").append("type", "$scores.type"), sum("activitats",1)),
				//match(gte("examen", 40)),
				//project(new Document("state", 1).append("city", 1).append("_id", 0)),				
				limit(10)
				
		)).forEach((Document doc) -> System.out.println(doc.toJson()));
	}	
	
	public static void consulta3A() {
		
		/*
		  Fes un programa que mostri la mitjana de notes de cadascun dels tipus
d'activitat que hi ha.
		 */
	
		
		coll.aggregate(Arrays.asList(
				unwind("$scores"),
				//match(eq("scores.type", "exam")),
				//group(new Document("activitat","$scores.type")),
				group(new Document("activitat","$scores.type"),avg("mitjana","$scores.score")),

				//match(gte("examen", 40)),
				//project(new Document("state", 1).append("city", 1).append("_id", 0)),				
				limit(10)
				
		)).forEach((Document doc) -> System.out.println(doc.toJson()));
	}	
	
	public static void consulta4A() {
		
		/*
		 Per a cada alumnes de la col·lecció students volem calcular la seva nota
		final utilitzant un programa en Java.
		
		La nota es calcula de la següent manera:
		
		60% de la mitjana de les notes dels exàmens, un 30% de la mitjana de la nota
		dels deures i un 10% de la mitjana de la nota dels testos.
		
		Un estudiant ha d'haver tret almenys un 40 de mitjana als exàmens per superar
		el curs, en cas contrari tindrà una nota màxima de 40.
		
		Fes un programa que mostri la nota final de cada estudiant
		 */
	
		//double examenMitjana = 0;
		coll.aggregate(Arrays.asList(
				unwind("$scores"),
				//match(eq("scores.type", "exam")),
				//group(new Document("activitat","$scores.type")),
				group(new Document("id","$_id").append("type", "$scores.type"),avg("mitjana","$scores.score")),
				sort(new Document("_id.id", 1)),
				//match(gte("mitjana", 40)),
				//project(new Document("state", 1).append("city", 1).append("_id", 0)),				
				limit(15)
				
		)).forEach((Document doc) -> {
			contador ++;

			int id =0;
			//System.out.println(doc.toJson());
			Document doc2= doc.get("_id", Document.class);
			id = doc2.get("id", Integer.class);
			//doc.get("mitjana", Double.class)
			//System.out.println(id);
			if (doc2.get("type").equals("exam")) {
				examenMitjana  =  (double) doc.get("mitjana")*0.60;
			}else if (doc2.get("type").equals("quiz")) {
				deuresMitjana  =  (double) doc.get("mitjana")*0.30;
			}else {
				testosMitjana  =  (double) doc.get("mitjana")*0.10;
			}
			
			/*if (id != student.getId()) {
				// mostrar resultat
				student = new Student();
			}*/
			
			
			if (contador==3) {
				contador = 0;

				if (examenMitjana<40) {
					notaFinal = testosMitjana + deuresMitjana + examenMitjana;
					notaFinal = notaFinal >=40 ? 40 : notaFinal; 
					System.out.println("Alumne " + id + " nota final  " + notaFinal + " examen suspes");					
				}else {
					notaFinal = examenMitjana + deuresMitjana + testosMitjana;
					System.out.println("Alumne " + id + " nota final  " + notaFinal);				
				}
			}
			
		});
	}	
	
	private static void consulta5A() {
		/* Fes un programa que demani el nom d'un estudiant i mostri la seva nota final,
		 així com les notes i tipus de cadascuna de les activitats que ha fet. */
		
		// Corliss Zuk
		String nom = "Corliss Zuk";
		System.out.println("nota Final  " + obtenirNotaFinalAlumne(nom));
		System.out.println(EXAMEN + " " +  examenMitjana);
		System.out.println(ACTIVITATS + " " +  deuresMitjana);
		System.out.println(TESTOS + " " + testosMitjana);

		
		/*coll.aggregate(Arrays.asList(
				match(eq("name", nom)),
				unwind("$scores")
				//match(eq("scores.type", "exam")),
				//group(new Document("activitat","$scores.type")),
				//group(new Document("activitat","$scores.type"),avg("mitjana","$scores.score")),

				//match(gte("examen", 40)),
				//project(new Document("state", 1).append("city", 1).append("_id", 0)),				
				//limit(10)
				
		)).forEach((Document doc) -> System.out.println(doc.toJson()));*/
		
	}
	
	private static String obtenirNotaFinalAlumne(String nomAlumne) {
		String s = "";
		
		coll.aggregate(Arrays.asList(
				unwind("$scores"),
				match(eq("name", nomAlumne)),
				group(new Document("id","$_id").append("type", "$scores.type"),avg("mitjana","$scores.score")),
				sort(new Document("_id.id", 1))
				//match(gte("mitjana", 40)),
				//project(new Document("state", 1).append("city", 1).append("_id", 0)),				
				//limit(15)
				
		)).forEach((Document doc) -> {
			contador ++;

			int id =0;
			Document doc2= doc.get("_id", Document.class);
			id = doc2.get("id", Integer.class);
			
			if (doc2.get("type").equals("exam")) {
				examenMitjana  =  (double) doc.get("mitjana")*0.60;
			}else if (doc2.get("type").equals("quiz")) {
				deuresMitjana  =  (double) doc.get("mitjana")*0.30;
			}else {
				testosMitjana  =  (double) doc.get("mitjana")*0.10;
			}
			
			
			
			if (contador==3) {
				contador = 0;
				if (examenMitjana<40) {
					notaFinal = testosMitjana + deuresMitjana + examenMitjana;
					notaFinal = notaFinal >=40 ? 40 : notaFinal; 
					
					//System.out.println("Alumne " + id + " nota final  " + notaFinal + " examen suspes");					
				}else {
					notaFinal = examenMitjana + deuresMitjana + testosMitjana;
					//System.out.println("Alumne " + id + " nota final  " + notaFinal);				
				}
			}
			
		});
		s = notaFinal + "";
		return s;
	}
	
	private static void consulta6A() {
		// Per a la col·lecció grades volem saber la mitjana de la nota dels exàmens
		// per a cadascuna de les classes que hi ha. Quina és la classe que ha tret,
		// de mitjana, una millor nota als exàmens?
		
		coll.aggregate(Arrays.asList(
				unwind("$scores"),
				match(eq("scores.type", "exam")),
				group(new Document("classe","$class_id"),avg("mitjana","$scores.score")),
				sort(new Document("mitjana", -1)),
				limit(10)
				
		)).forEach((Document doc) -> {
			if (primerCop) {
				Document doc2= doc.get("_id", Document.class);
				idClasse = doc2.get("classe", Integer.class);
				primerCop = false;	
				notaMaxima = (double) doc.get("mitjana");			
			}
			System.out.println(doc.toJson());			
		});
		
		System.out.println("idClasse > " + idClasse + "  Mitjana mes alta > " + notaMaxima);
	}
	
	private static void consulta7A() {
		// A la col·lecció grades un mateix estudiant pot estar a diverses classes.
		// Volem fer un programa que, donat el codi d'un estudiant, ens mostri el
		// tipus d'activitat i nota de tot el que ha fet, independentment de a quina
		// classe hagi obtingut les notes.
		
	}
	
	
}

//		coll.find(filter).projection(projection).forEach((Document doc) -> System.out.println(doc.toJson()));
//      {$project : {pop:1, state:1}}




package com.example.joker.aplicacioanimacions;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private SeekBar seekBar;
    private TextView textView;
    private ImageView imageView;
    private Button fadeIn,fadeOut,zoomIn,zoomOut,leftRight,topBottom,bounce,flash,rotate,several;
    private Animation generalAnimation;
    private Animation animationFadeIn,animationFadeOut,animationBounce,animationRotate,animationZoomIn,animationZoomOut,animationMoveRight,animationTopBottom,animationFlash,animationRandom;

    private long duration;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrieveViews();
        createAnimations();
        setEvents();
    }

    private void createAnimations(){
        animationFadeOut = AnimationUtils.loadAnimation(this,R.anim.fade_out);
        animationFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.INVISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationFadeIn = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationBounce = AnimationUtils.loadAnimation(this,R.anim.bounce);
        animationBounce.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        animationRotate = AnimationUtils.loadAnimation(this,R.anim.rotate);
        animationRotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationZoomIn = AnimationUtils.loadAnimation(this,R.anim.zoom_in);
        animationZoomIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationZoomOut = AnimationUtils.loadAnimation(this,R.anim.zoom_out);
        animationZoomOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationMoveRight = AnimationUtils.loadAnimation(this,R.anim.move_right);
        animationMoveRight.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationTopBottom = AnimationUtils.loadAnimation(this,R.anim.top_bottom);
        animationTopBottom.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationFlash = AnimationUtils.loadAnimation(this,R.anim.flash);
        animationFlash.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationRandom = AnimationUtils.loadAnimation(this,R.anim.random);
        animationRandom.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
                textView.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });




    }
    private void setEvents(){
        fadeOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationFadeOut.setDuration(duration);
                imageView.startAnimation(animationFadeOut);
                generalAnimation=animationFadeOut;
                textView.setText("RUNNING");

            }
        });

        fadeIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationFadeIn.setDuration(duration);
                imageView.startAnimation(animationFadeIn);
                generalAnimation = animationFadeIn;
                textView.setText("RUNNING");

            }
        });
        bounce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationBounce.setDuration(duration);
                imageView.startAnimation(animationBounce);
                generalAnimation = animationBounce;
                textView.setText("RUNNING");

            }
        });

        rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationRotate.setDuration(duration);
                imageView.startAnimation(animationRotate);
                generalAnimation = animationRotate;
                textView.setText("RUNNING");

            }
        });

        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationZoomIn.setDuration(duration);
                imageView.startAnimation(animationZoomIn);
                generalAnimation = animationZoomIn;
                textView.setText("RUNNING");

            }
        });

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationZoomOut.setDuration(duration);
                imageView.startAnimation(animationZoomOut);
                generalAnimation = animationZoomOut;
                textView.setText("RUNNING");

            }
        });

        leftRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationMoveRight.setDuration(duration);
                imageView.startAnimation(animationMoveRight);
                generalAnimation = animationMoveRight;
                textView.setText("RUNNING");

            }
        });

        topBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationTopBottom.setDuration(duration);
                imageView.startAnimation(animationTopBottom);
                generalAnimation = animationTopBottom;
                textView.setText("RUNNING");

            }
        });


        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationFlash.setDuration(duration);
                imageView.startAnimation(animationFlash);
                generalAnimation = animationFlash;
                textView.setText("RUNNING");


            }
        });

        several.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationRandom.setDuration(duration);
                imageView.startAnimation(animationRandom);
                generalAnimation = animationRandom;
                textView.setText("RUNNING");
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (generalAnimation!=null){
                    duration = i*50;
                    generalAnimation.setDuration(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



    }

    private void retrieveViews(){
        imageView = findViewById(R.id.imageView);
        seekBar = findViewById(R.id.seekBar);
        textView = findViewById(R.id.textView);
        fadeIn = findViewById(R.id.fadeIn);
        fadeOut = findViewById(R.id.fadeOut);
        zoomIn = findViewById(R.id.zoomIn);
        zoomOut = findViewById(R.id.zoomOut);
        leftRight = findViewById(R.id.leftRight);
        topBottom = findViewById(R.id.topBottom);
        bounce = findViewById(R.id.bounce);
        flash = findViewById(R.id.flash);
        rotate = findViewById(R.id.rotate);
        several = findViewById(R.id.several);
    }
}

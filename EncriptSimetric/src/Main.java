import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.sun.javafx.css.StyleCache.Key;

public class Main {
	
	 private final static String ALG = "AES";
	 
	 static String pass = "TheBestSecretKey";
	   // static String pass2 = "TheBestSecretKel";
	 private static String nomFitxer = "fitxer.txt";
	    
	    private static final byte[] keyValue = pass.getBytes();
	    //private static final byte[] keyValue2 = pass2.getBytes();

	 
	public static void main(String[] args) throws Exception {
		String s2 = "";

		
		String s [] = nomFitxer.split("\\.");
		
		//System.out.println(s.length);
		
		if (s[1].equals("txt")) {
			encriptaFitxer(nomFitxer,keyValue);
		}else {
	
		}
		

		System.out.println("Fi exercici encriptat asimetric");
	}
	
	public static void encriptaFitxer(String fileClean,byte[] keyValue2) throws Exception{
		String s = "";
		String pass = "";
		String contingutEncriptat = "";
		FileReader f = new FileReader (fileClean);
		
		try (BufferedReader lector = new BufferedReader(f);) {
			while ((s=lector.readLine()) != null)
				pass = pass + s;
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		contingutEncriptat = encrypt(pass,keyValue2);
		System.out.println(contingutEncriptat);
		
	}
	
	public static String encrypt(String data, byte []  keyValue) throws Exception {
		
        SecretKeySpec key = generateKey(keyValue);
        Cipher c = Cipher.getInstance(ALG);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encVal);
    }

	
	
	
    public static byte[] encryptBytes(String data) throws Exception {
        SecretKeySpec key = generateKey(keyValue);
        Cipher c = Cipher.getInstance(ALG);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());

        return encVal;
    }
    
    private static SecretKeySpec generateKey(byte[] keyValue) throws Exception {
        return new SecretKeySpec(keyValue, ALG);

    }



}

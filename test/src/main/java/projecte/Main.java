package projecte;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import sakila_online.Customer;
import sakila_online.Trader;


public class Main {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Trader.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			//int id = customerPattern.getId();

			
			session.beginTransaction();
			//Mercader mercader1 = new Mercader("Joker22","Harre22","jokerNickName2");
			//session.save(mercader1);
			//System.out.println(mercader1.getId());
			//System.out.println(mercader1.toString());

			System.out.println("--------");
			Trader customer = session.get(Trader.class, 14);
			System.out.println(customer.toString());
			
			session.getTransaction().commit();
			
		}finally {
			session.close();
			factory.close();
		}

	}

}

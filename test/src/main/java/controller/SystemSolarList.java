package controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import sakila_online.SystemSolar;

@ManagedBean
@ApplicationScoped
public class SystemSolarList {
	private List<SystemSolar> attendees;

	public SystemSolarList() {
		attendees = new ArrayList<>();

		attendees.add(new SystemSolar("Planeta1"));
		attendees.add(new SystemSolar("Planeta2"));
		attendees.add(new SystemSolar("Planeta3"));
	}

	public List<SystemSolar> getAttendees() {
		return attendees;
	}

	public int getSize() {
		return attendees.size();
	}
}
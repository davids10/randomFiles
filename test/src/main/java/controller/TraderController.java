package controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import dao.HibernateUtil;
import sakila_online.Trader;


@ManagedBean
@ViewScoped
public class TraderController {
	
	public void createTrader(Trader trader) {

		SessionFactory factory = HibernateUtil.getInstance().getSessionFactory();
		Session session = factory.getCurrentSession();
		try {	
			session.beginTransaction();
			
			/*System.out.println("--------");
			Trader customer = session.get(Trader.class, 14);
			System.out.println(customer.toString());*/
			
			Trader t = new Trader(trader.getFirstName(),trader.getLastName(),trader.getNickName());
			session.save(t);
			

			t.setActivityDate(trader.getActivityDate());

			System.out.println("---->>> " + t.toString());
			
			session.getTransaction().commit();
			


		} finally {
			session.close();
			factory.close();
		}

	}

	
}

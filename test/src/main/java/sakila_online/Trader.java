package sakila_online;

import java.sql.Date;
import java.time.LocalDate;

import javax.faces.bean.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import dao.HibernateUtil;

@ManagedBean
@Entity
@Table(name = "Traders")
public class Trader {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int id;

	@Column(name = "FirstName")
	private String firstName;

	@Column(name = "LastName")
	private String lastName;

	@Column(name = "NickName")
	private String nickName;

	@Column(name = "DefunctDate")
	private LocalDate defunctDate;
	
	@Transient
	private String activityDate; 

	
	
	public Trader() {
		//defunctDate = LocalDate.now();
	}

	/*public Trader(String firstName, String lastName, String nickName,LocalDate localDate) {
		this();
		this.firstName = firstName;
		this.lastName = lastName;
		this.nickName = nickName;
		this.defunctDate = localDate;
	}*/
	
	public String getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public Trader(String firstName, String lastName, String nickName) {
		this();
		this.firstName = firstName;
		this.lastName = lastName;
		this.nickName = nickName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public LocalDate getDefunctDate() {
		return defunctDate;
	}

	public void setDefunctDate(LocalDate defunctDate) {
		this.defunctDate = defunctDate;
	}

	@Override
	public String toString() {
		return "Trader [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", nickName=" + nickName
				+ ", defunctDate=" + defunctDate + ", activityDate=" + activityDate + "]";
	}

	


}
